package date.hb.com.wdiget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by A on 2017/3/8.
 */

public class TitleBar extends RelativeLayout implements TitleBarI{

    private Button left_btn;
    private TextView title_txv;
    private Button right_btn;

    private LayoutParams leftParams;
    private LayoutParams titleParams;
    private LayoutParams rightParams;

    private int titleWidth;
    private int titleHeight;

    private WindowManager wm ;

    //title
    private int titleColor;
    private String title;
    private float titleSize;
    //left
    private int leftColor;
    private String leftStr;
    private float leftSize;
    //right
    private int rightColor;
    private String rightStr;
    private float rightSize;

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(attrs);
        initView(context);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(attrs);
        initView(context);
    }

    private void initAttrs(AttributeSet attrs){
        if(attrs!=null){
            TypedArray array = null;
            try{
                //读取属性
                array = getContext().obtainStyledAttributes(attrs,R.styleable.TitleBar);
                //读取title属性
                this.title = array.getString(R.styleable.TitleBar_title);
                this.titleSize = array.getDimension(R.styleable.TitleBar_titleTextSize,15);
                this.titleColor = array.getColor(R.styleable.TitleBar_titleTextColor,Color.BLACK);
                //读取left属性
                this.leftStr = array.getString(R.styleable.TitleBar_leftText);
                this.leftColor = array.getColor(R.styleable.TitleBar_leftTextColor,Color.BLACK);
                this.leftSize = array.getDimension(R.styleable.TitleBar_leftTextSize,20);
                //读取right属性
                this.rightStr = array.getString(R.styleable.TitleBar_rightText);
                this.rightColor = array.getColor(R.styleable.TitleBar_rightTextColor,Color.BLACK);
                this.rightSize = array.getDimension(R.styleable.TitleBar_rightTextSize,25);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private void initView(Context context){

        wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        //设置left
        left_btn = new Button(context);
        if(leftStr==null) {
            left_btn.setText("←");
        }else{
            left_btn.setText(leftStr);
        }
        left_btn.setBackgroundColor(Color.TRANSPARENT);
        left_btn.setTextColor(leftColor);
        left_btn.setTextSize(leftSize);
        left_btn.setGravity(Gravity.CENTER);
        leftParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT );
        leftParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT,TRUE);
        addView(left_btn,leftParams);
        //设置title
        title_txv = new TextView(context);
        if(title==null) {
            title_txv.setText("标题");
        }else{
            title_txv.setText(title);
        }
        title_txv.setTextColor(titleColor);
        title_txv.setTextSize(titleSize);
        title_txv.setBackgroundColor(Color.TRANSPARENT);
        titleParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        titleParams.addRule(RelativeLayout.CENTER_IN_PARENT,TRUE);
        addView(title_txv,titleParams);
        //设置right
        right_btn = new Button(context);
        if(rightStr==null) {
            right_btn.setText("···");
        }else{
            right_btn.setText(rightStr);
        }
        right_btn.setTextSize(rightSize);
        right_btn.setTextColor(rightColor);
        right_btn.setBackgroundColor(Color.TRANSPARENT);
        right_btn.setGravity(Gravity.CENTER);
        rightParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        rightParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,TRUE);
        addView(right_btn,rightParams);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //获取宽度的模式和大小
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        //获取高度的模式和大小
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        this.titleHeight = measureHeight(heightMode,height);
        this.titleWidth = measureWidth(widthMode,width);

        this.setMeasuredDimension(titleWidth,titleHeight);
    }

    private int measureWidth(int mode ,int width){
        switch(mode){
            case MeasureSpec.UNSPECIFIED:
                break;
            case MeasureSpec.AT_MOST://warp_content
                titleWidth = width;
//                DisplayMetrics dm = new DisplayMetrics();
//                wm.getDefaultDisplay().getMetrics(dm);
//                titleWidth = dm.widthPixels;
                break;
            case MeasureSpec.EXACTLY://match_parent或者具体的数字
                titleWidth = width;
                break;
        }
        return this.titleWidth;
    }

    private int measureHeight(int mode , int height){
        switch(mode){
            case MeasureSpec.UNSPECIFIED:
                break;
            case MeasureSpec.AT_MOST:
                titleHeight = dp2px(50);
                break;
            case MeasureSpec.EXACTLY:
                titleHeight = height;
                break;
        }
        return this.titleHeight;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void setTitle(String title) {
        this.title_txv.setText(title);
    }

    @Override
    public void setLeftBack() {
        this.left_btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                ((Activity)getContext()).finish();
            }
        });
    }

    @Override
    public void setLeftAction(OnClickListener onClickListener) {
        this.left_btn.setOnClickListener(onClickListener);
    }

    @Override
    public View getLeftButton() {
        return this.left_btn;
    }

    @Override
    public void setLeftVisible(boolean visible) {
        if(visible) {
            this.left_btn.setVisibility(View.VISIBLE);
        }else{
            this.left_btn.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setRightAction(OnClickListener onClickListener) {
        this.right_btn.setOnClickListener(onClickListener);
    }

    @Override
    public void setRightVisible(boolean visible) {
        if(visible){
            this.right_btn.setVisibility(View.VISIBLE);
        }else{
            this.right_btn.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public View getRightButton() {
        return this.right_btn;
    }


}

