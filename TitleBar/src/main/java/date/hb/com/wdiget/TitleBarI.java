package date.hb.com.wdiget;

import android.view.View;

/**
 * Created by A on 2017/3/8.
 */

public interface TitleBarI {
    public void setTitle(String title);
    public void setLeftBack();
    public void setLeftAction(View.OnClickListener onClickListener);
    public View getLeftButton();
    public void setLeftVisible(boolean visible);
    public void setRightAction(View.OnClickListener onClickListener);
    public void setRightVisible(boolean visible);
    public View getRightButton();
}
