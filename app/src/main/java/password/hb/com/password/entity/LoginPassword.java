package password.hb.com.password.entity;

/**
 * Created by A on 2017/3/7.
 */

public class LoginPassword {
    private String password;

    public LoginPassword(){}

    public LoginPassword(String password){
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
