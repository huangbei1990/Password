package password.hb.com.password.model;

import java.util.ArrayList;
import java.util.List;

import password.hb.com.password.R;
import password.hb.com.password.entity.APP;

/**
 * Created by A on 2017/1/13.
 */

public class APPModel {

    private static APPModel instance;

    private List<APP> appList;

    private APPModel(){
        init();
    }

    public static APPModel getInstance(){
        if(instance==null){
            instance = new APPModel();
        }
        return instance;
    }

    private void init(){
        appList = new ArrayList<>();
        APP app1 = new APP("百度", R.mipmap.baidu);
        APP app2 = new APP("邮箱", R.mipmap.email);
        APP app3 = new APP("Facebook", R.mipmap.facebook);
        APP app4 = new APP("Google", R.mipmap.google);
        APP app5 = new APP("QQ", R.mipmap.qq);
        APP app6 = new APP("微信", R.mipmap.wechat);
        APP app7 = new APP("微博", R.mipmap.weibo);
        APP app8 = new APP("雅虎", R.mipmap.yahoo);
        APP app9 = new APP("优酷",R.mipmap.youku);
        APP app10 = new APP("土豆",R.mipmap.tudou);
        APP app11 = new APP("爱奇艺",R.mipmap.aqy);
        APP app12 = new APP("其他",R.mipmap.other);
        appList.add(app1);
        appList.add(app2);
        appList.add(app3);
        appList.add(app4);
        appList.add(app5);
        appList.add(app6);
        appList.add(app7);
        appList.add(app8);
        appList.add(app9);
        appList.add(app10);
        appList.add(app11);
        appList.add(app12);
    }

    public List<APP> getAPPList(){
        return this.appList;
    }
}
