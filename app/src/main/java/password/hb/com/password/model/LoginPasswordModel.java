package password.hb.com.password.model;

import android.content.Context;

import password.hb.com.password.entity.LoginPassword;
import password.hb.com.password.util.SharedPreUtil;

/**
 * Created by A on 2017/3/7.
 */

public class LoginPasswordModel {

    private LoginPassword loginPassword;

    private Context context;

    private SharedPreUtil sharedPreUtil;

    public static boolean isHaveLoginPassword = false;

    public LoginPasswordModel(Context context){
        this.context = context;
        sharedPreUtil = new SharedPreUtil(context);
        loginPassword = (LoginPassword) sharedPreUtil.get(LoginPassword.class);
        if(loginPassword != null && loginPassword.getPassword()!=null){
            isHaveLoginPassword = true;
        }
    }

    public String getLoginPassword(){
        if(this.loginPassword == null){
            return null;
        }else{
            return this.loginPassword.getPassword();
        }
    }

    public boolean verify(String password){
        if(this.loginPassword == null || this.loginPassword.getPassword() == null){
            return false;
        }
        if(loginPassword.getPassword().equals(password)){
            return true;
        }else{
            return false;
        }
    }

    public void savePassword(String password){
        if(loginPassword == null){
            loginPassword = new LoginPassword(password);
        }else{
            loginPassword.setPassword(password);
        }
        sharedPreUtil.save(loginPassword,LoginPassword.class);
    }
}
