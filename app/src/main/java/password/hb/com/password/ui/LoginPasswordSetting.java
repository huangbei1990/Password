package password.hb.com.password.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import password.hb.com.password.R;
import password.hb.com.password.constant.DeliverConstant;
import password.hb.com.password.constant.InputConstant;
import password.hb.com.password.model.LoginPasswordModel;

/**
 * Created by A on 2017/3/7.
 */

public class LoginPasswordSetting extends BaseActivity implements View.OnClickListener{

    private EditText old_pwd;
    private EditText new_pwd;
    private EditText repeate_new_pwd;
    private Button btn_save;
    private ImageView img_pwd;

    private LoginPasswordModel loginPasswordModel;

    private String pattern;

    @Override
    protected void onInitView(Bundle args0) {
        setContentViewWithActionBar(R.layout.password_setting);
        titlebar.setRightVisible(false);
        old_pwd = (EditText)findViewById(R.id.old_password);
        new_pwd = (EditText)findViewById(R.id.new_password);
        repeate_new_pwd = (EditText)findViewById(R.id.repeat_new_password);
        btn_save = (Button) findViewById(R.id.btn_save);
        img_pwd = (ImageView)findViewById(R.id.iv_pwd);
        img_pwd.setBackgroundResource(R.mipmap.connect_pwd_hide);

        old_pwd.setInputType(InputConstant.NUM_HIDE_PWD);
        new_pwd.setInputType(InputConstant.NUM_HIDE_PWD);
        repeate_new_pwd.setInputType(InputConstant.NUM_HIDE_PWD);
    }

    @Override
    protected void onSetEvent() {
        titlebar.setLeftBack();
        btn_save.setOnClickListener(this);
        img_pwd.setOnClickListener(this);
    }

    @Override
    protected void onInitData() {
        titlebar.setTitle(getString(R.string.setting));
        pattern = this.getIntent().getExtras().getString(DeliverConstant.resetOrSetting);
        if(pattern != null){
            if(pattern.equals(DeliverConstant.resetPassword)){//重置密码
                old_pwd.setHint(R.string.old_password);
                old_pwd.setVisibility(View.VISIBLE);
                new_pwd.setHint(R.string.new_password);
                repeate_new_pwd.setHint(R.string.repeate_new_password);
            }else{//设置密码
                old_pwd.setVisibility(View.GONE);
                new_pwd.setHint(R.string.password);
                repeate_new_pwd.setHint(R.string.repeate_password);
            }
        }
        loginPasswordModel = new LoginPasswordModel(this);
    }

    @Override
    public void onClick(View v) {
        if(v==btn_save){
            if(pattern.equals(DeliverConstant.settingPassword)){
                if(true == verifyNewAndRepeate()){
                    loginPasswordModel.savePassword(repeate_new_pwd.getText().toString());
                }
            }else if(pattern.equals(DeliverConstant.resetPassword)){
                if(true == verifyNewAndRepeate() && true == verifyOld()){
                    loginPasswordModel.savePassword(repeate_new_pwd.getText().toString());
                }
            }
            this.finish();
        }else if(v==img_pwd){
            if(repeate_new_pwd.getInputType() == InputConstant.NUM_HIDE_PWD){
                repeate_new_pwd.setInputType(InputConstant.NUM_SHOW_PWD);
                old_pwd.setInputType(InputConstant.NUM_SHOW_PWD);
                new_pwd.setInputType(InputConstant.NUM_SHOW_PWD);
                img_pwd.setBackgroundResource(R.mipmap.connect_pwd_show);
            }else{
                repeate_new_pwd.setInputType(InputConstant.NUM_HIDE_PWD);
                old_pwd.setInputType(InputConstant.NUM_HIDE_PWD);
                new_pwd.setInputType(InputConstant.NUM_HIDE_PWD);
                img_pwd.setBackgroundResource(R.mipmap.connect_pwd_hide);
            }
        }
    }

    //验证密码和重复密码
    private boolean verifyNewAndRepeate(){
        String newPassword = new_pwd.getText().toString();
        String repeatePassword = repeate_new_pwd.getText().toString();
        if(newPassword==null || newPassword.equals("")){
            Toast.makeText(this,R.string.error_newpass_input,Toast.LENGTH_SHORT).show();
            return false;
        }
        if(repeatePassword==null || repeatePassword.equals("")){
            Toast.makeText(this,R.string.error_repeate_input,Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!repeatePassword.equals(newPassword)){
            Toast.makeText(this,R.string.error_pass_notsame,Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    //验证旧密码
    private boolean verifyOld(){
        String oldPassword = old_pwd.getText().toString();
        if(oldPassword==null || oldPassword.equals("")){
            Toast.makeText(this,R.string.error_oldpass_input,Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!loginPasswordModel.getLoginPassword().equals(oldPassword)){
            Toast.makeText(this,R.string.error_pass_verify,Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
