package password.hb.com.password.ui;

/**
 * Created by A on 2017/3/6.
 */

public interface LoginViewI {
    public void clearPassword();
    public void loginSucess();
    public void logFailed();
}
