package password.hb.com.password.widget;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import password.hb.com.password.R;

/**
 * Created by A on 2017/3/7.
 */

public class PassPopuMenu extends PopupMenu {

    private Context context;

    public PassPopuMenu(Context context, View anchor) {
        super(context, anchor);
        initPopuMenu(context);
    }

    public PassPopuMenu(Context context, View anchor, int gravity) {
        super(context, anchor, gravity);
        initPopuMenu(context);
    }

    private void initPopuMenu(Context context){
        this.getMenuInflater().inflate(R.menu.popu_menu,this.getMenu());
        this.context = context;
    }


    public void setOnItemClickListener(final OnMenuItemClickListener onItemClickListener){
        this.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onItemClickListener.onMenuItemClick(item);
                PassPopuMenu.this.dismiss();
                return true;
            }
        });
    }

    public void remove(int resId){
        this.getMenu().removeItem(resId);
    }
}
