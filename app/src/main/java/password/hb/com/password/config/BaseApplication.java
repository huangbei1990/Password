package password.hb.com.password.config;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import password.hb.com.password.entity.Password;

/**
 * Created by A on 2016/12/6.
 */

public class BaseApplication extends Application{

    @Override
    public void onCreate() {// 程序入口方法
        //初始化数据库
        Configuration.Builder builder = new Configuration.Builder(this);
        builder.addModelClass(Password.class);
        ActiveAndroid.initialize(builder.create());
    }
}
