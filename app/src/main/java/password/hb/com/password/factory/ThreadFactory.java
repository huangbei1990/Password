package password.hb.com.password.factory;

public class ThreadFactory {
	static ThreadPoolProxy	mNormalPool;	// 只需初始化一次就行了
	static ThreadPoolProxy	mDownLoadPool;	// 只需初始化一次就行了
	static ScheduledThreadPool mScheduledPool;
	static SingleThreadPool mSinglePool;

	/**创建了一个普通的线程池*/
	public static ThreadPoolProxy getNormalPool() {
		if (mNormalPool == null) {
			synchronized (ThreadFactory.class) {
				if (mNormalPool == null) {
					mNormalPool = new ThreadPoolProxy(5, 10, 3000);
				}
			}
		}
		return mNormalPool;
	}

	/**创建了一个下载的线程池*/
	public static ThreadPoolProxy getDownLoadPool() {
		if (mDownLoadPool == null) {
			synchronized (ThreadFactory.class) {
				if (mDownLoadPool == null) {
					mDownLoadPool = new ThreadPoolProxy(3, 3, 3000);
				}
			}
		}
		return mDownLoadPool;
	}

	/**创建一个执行周期性任务的线程池*/
	public static ScheduledThreadPool getScheduledPool(){
		if (mScheduledPool == null){
			synchronized (ScheduledThreadPool.class){
				if(mScheduledPool == null){
					mScheduledPool = new ScheduledThreadPool(5);
				}
			}
		}
		return mScheduledPool;
	}

	//创建一个单线程池
	public static SingleThreadPool getSinglePool(){
		if(mSinglePool == null){
			synchronized (SingleThreadPool.class){
				if(mSinglePool == null){
					mSinglePool = new SingleThreadPool();
				}
			}
		}
		return mSinglePool;
	}


}
