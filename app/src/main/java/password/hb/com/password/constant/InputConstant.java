package password.hb.com.password.constant;

import android.text.InputType;

/**
 * Created by A on 2017/3/9.
 */

public class InputConstant {
    public static final int HIDE_PWD = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;
    public static final int SHOW_PWD = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;

    public static final int NUM_HIDE_PWD = InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_VARIATION_PASSWORD;
    public static final int NUM_SHOW_PWD = InputType.TYPE_CLASS_NUMBER|InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
}
