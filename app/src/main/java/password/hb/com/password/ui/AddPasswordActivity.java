package password.hb.com.password.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import password.hb.com.password.R;
import password.hb.com.password.constant.DeliverConstant;
import password.hb.com.password.constant.InputConstant;
import password.hb.com.password.entity.APP;
import password.hb.com.password.entity.Password;
import password.hb.com.password.model.APPModel;
import password.hb.com.password.model.PasswordModel;
import password.hb.com.password.util.AndroidVersion;
import password.hb.com.password.util.ToolBarColorUtil;
import password.hb.com.password.widget.ComponentImage;

/**
 * Created by A on 2016/12/6.
 * 增加密码
 */

public class AddPasswordActivity extends BaseActivity implements View.OnClickListener{

    private EditText userNameEdit;
    private EditText pwdEdit;
    private ImageView pwdIv;
    private EditText tagEdit;
    private Button addBtn;
    private ComponentImage componentImg;
    private Password pwdEntity;


    @Override
    protected void onInitView(Bundle args0) {
        this.setContentViewWithActionBar(R.layout.passworld_add_activity);
        userNameEdit = (EditText) this.findViewById(R.id.et_username);
        pwdEdit = (EditText)this.findViewById(R.id.et_pwd);
        pwdEdit.setInputType(InputConstant.HIDE_PWD);
        tagEdit = (EditText)this.findViewById(R.id.et_tag);
        addBtn = (Button) this.findViewById(R.id.b_entry);
        componentImg = (ComponentImage) this.findViewById(R.id.componentImage);
        pwdIv = (ImageView) this.findViewById(R.id.iv_pwd);

        initComponentImage();
        titlebar.setRightVisible(false);
    }

    @Override
    protected void onSetEvent() {
        titlebar.setLeftBack();
        addBtn.setOnClickListener(this);
        pwdIv.setOnClickListener(this);
    }

    @Override
    protected void onInitData() {
        Bundle bundle = this.getIntent().getExtras();
        if(bundle!=null) {
            int index = bundle.getInt(DeliverConstant.psswordIndex);
            pwdEntity = PasswordModel.getInstance().getByIndex(index);
            if (pwdEntity != null) {
                userNameEdit.setText(pwdEntity.getUserName());
                pwdEdit.setText(pwdEntity.getPassword());
                if (pwdEntity.getTag() != null) {
                    tagEdit.setText(pwdEntity.getTag());
                }
                componentImg.setSelectedImage(pwdEntity.getAppName());
            }
            titlebar.setTitle("编辑");
        }else{
            titlebar.setTitle("添加");
        }
    }

    private void initComponentImage(){
        componentImg.addImage(APPModel.getInstance().getAPPList());
        componentImg.setOnClickListener(new ComponentImage.ClickListener() {
            @Override
            public void onClick(APP app,int index) {
//                Toast.makeText(AddPasswordActivity.this,"您点击的是"+index+" "+app.getName(),Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        if(v == pwdIv){
            if(pwdEdit.getInputType() == InputConstant.HIDE_PWD){
                pwdEdit.setInputType(InputConstant.SHOW_PWD);
                pwdIv.setBackgroundResource(R.mipmap.connect_pwd_show);
            }else{
                pwdEdit.setInputType(InputConstant.HIDE_PWD);
                pwdIv.setBackgroundResource(R.mipmap.connect_pwd_hide);
            }
        }else if(v == addBtn){
            String userName = userNameEdit.getText().toString();
            String password = pwdEdit.getText().toString();
            String tag = tagEdit.getText().toString();
            int selectedIndex = componentImg.getSelectedIndex();
            if(userName==null || userName.equals("")){
                Toast.makeText(this,R.string.error_enter_username,Toast.LENGTH_SHORT).show();
                return;
            }

            if(password==null || password.equals("")){
                Toast.makeText(this,R.string.error_enter_password,Toast.LENGTH_SHORT).show();
                return;
            }

            if(selectedIndex == ComponentImage.NO_IMAGE_SELECTED){
                Toast.makeText(this,R.string.error_choose_type,Toast.LENGTH_SHORT).show();
                return;
            }
            APP app = componentImg.getSelectedApp();
            if(pwdEntity != null){
                pwdEntity.setUserName(userName);
                pwdEntity.setAppImageId(app.getImageId());
                pwdEntity.setAppName(app.getName());
                pwdEntity.setPassword(password);
                pwdEntity.setTag(tag);
            }else{
                pwdEntity = new Password(app.getName(),userName,password,app.getImageId(),tag);
            }

            PasswordModel model = PasswordModel.getInstance();
            model.save(pwdEntity);
            this.finish();
        }
    }

}
