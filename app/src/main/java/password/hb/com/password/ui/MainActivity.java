package password.hb.com.password.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import password.hb.com.password.R;
import password.hb.com.password.model.LoginPasswordModel;
import password.hb.com.password.presenter.LgoinPresenterImpl;
import password.hb.com.password.presenter.LoginPresenterI;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,LoginViewI{

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private Button button7;
    private Button button8;
    private Button button9;
    private Button button0;

    private Button pwdDelete;

    private TextView password;
    private StringBuilder passBuilder = new StringBuilder();

    private LoginPresenterI loginPresenter;

    private LoginPasswordModel loginPasswordModel;

    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pass_login_activity);

        password = (TextView) findViewById(R.id.pwd_textview);

        pwdDelete = (Button) findViewById(R.id.pwd_delete);
        pwdDelete.setOnClickListener(this);
        pwdDelete.setVisibility(View.INVISIBLE);

        button0 = (Button) findViewById(R.id.btn_0);
        button1 = (Button) findViewById(R.id.btn_1);
        button2 = (Button) findViewById(R.id.btn_2);
        button3 = (Button) findViewById(R.id.btn_3);
        button4 = (Button) findViewById(R.id.btn_4);
        button5 = (Button) findViewById(R.id.btn_5);
        button6 = (Button) findViewById(R.id.btn_6);
        button7 = (Button) findViewById(R.id.btn_7);
        button8 = (Button) findViewById(R.id.btn_8);
        button9 = (Button) findViewById(R.id.btn_9);

        button0.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);

        loginPasswordModel = new LoginPasswordModel(this);
        loginPresenter = new LgoinPresenterImpl(loginPasswordModel);
        //如果没有设置密码就直接登录
        if(loginPasswordModel.getLoginPassword()==null){
            this.loginSucess();
        }

    }

    @Override
    public void onClick(View v) {

        pwdDelete.setVisibility(View.VISIBLE);
        count ++;
        if(v==button0){
            password.setText(password.getText()+"*");
            passBuilder.append(0);
        }else if(v==button1){
            password.setText(password.getText()+"*");
            passBuilder.append(1);
        }else if(v==button2){
            password.setText(password.getText()+"*");
            passBuilder.append(2);
        }else if(v==button3){
            password.setText(password.getText()+"*");
            passBuilder.append(3);
        }else if(v==button4){
            password.setText(password.getText()+"*");
            passBuilder.append(4);
        }else if(v==button5){
            password.setText(password.getText()+"*");
            passBuilder.append(5);
        }else if(v==button6){
            password.setText(password.getText()+"*");
            passBuilder.append(6);
        }else if(v==button7){
            password.setText(password.getText()+"*");
            passBuilder.append(7);
        }else if(v==button8){
            password.setText(password.getText()+"*");
            passBuilder.append(8);
        }else if(v==button9){
            password.setText(password.getText()+"*");
            passBuilder.append(9);
        }else if(v==pwdDelete){
            count = 0;
            passBuilder.delete(0,passBuilder.length()+1);
            password.setText("");
            pwdDelete.setVisibility(View.INVISIBLE);
        }

        if(count >= 4){
            loginPresenter.login(passBuilder.toString(),this);
            count = 0;
            passBuilder.delete(0,passBuilder.length()+1);
        }

    }

    @Override
    public void clearPassword() {
        password.setText("");
        pwdDelete.setVisibility(View.INVISIBLE);
    }

    @Override
    public void loginSucess() {
        Intent intent = new Intent(this,PasswordListActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void logFailed() {
        Toast.makeText(this,R.string.login_failed,Toast.LENGTH_SHORT).show();
    }
}
