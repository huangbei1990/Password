package password.hb.com.password.factory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by A on 2016/12/1.
 * 单线程池
 */

public class SingleThreadPool {

    private ExecutorService executorService;

    public SingleThreadPool(){
        executorService = Executors.newSingleThreadExecutor();
    }

    public void execute(Runnable runnable){
        this.executorService.execute(runnable);
    }
}
