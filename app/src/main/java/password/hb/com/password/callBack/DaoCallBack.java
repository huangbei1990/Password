package password.hb.com.password.callBack;

import java.util.List;

/**
 * Created by A on 2016/12/6.
 */

public interface DaoCallBack {
    public void onSuccess(String msg);
    public void onFailed(String msg);
}
