package password.hb.com.password.util;

/**
 * Created by A on 2017/1/17.
 */

public class AndroidVersion {

    public static final int Lollipop = 21;

    public static int getCurrentSystemVersion(){
        int currentapiVersion=android.os.Build.VERSION.SDK_INT;
        return currentapiVersion;
    }
}
