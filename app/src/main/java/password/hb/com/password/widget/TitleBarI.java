package password.hb.com.password.widget;

import android.view.View;

/**
 * Created by A on 2017/3/8.
 */

public interface TitleBarI {
    public void setTitle(String title);
    public void setLeftBack();
    public void setLeftAction(View.OnClickListener onClickListener);
    public void setLeftVisible(boolean visible);
    public void setRightAction(View.OnClickListener onClickListener);
    public void setRightVisible(boolean visible);
}
