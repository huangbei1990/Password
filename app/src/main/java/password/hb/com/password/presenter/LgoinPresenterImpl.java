package password.hb.com.password.presenter;

import password.hb.com.password.model.LoginPasswordModel;
import password.hb.com.password.ui.LoginViewI;

/**
 * Created by A on 2017/3/6.
 */

public class LgoinPresenterImpl implements LoginPresenterI {

    private LoginPasswordModel loginPasswordModel;

    public LgoinPresenterImpl(LoginPasswordModel loginPasswordModel){
        this.loginPasswordModel = loginPasswordModel;
    }

    @Override
    public void login(String password, LoginViewI loginview) {
        if(password.equals(loginPasswordModel.getLoginPassword())){
            loginview.loginSucess();
        }else{
            loginview.logFailed();
            loginview.clearPassword();
        }
    }
}
