package password.hb.com.password.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by A on 2017/3/7.
 */

public class TitleBar extends RelativeLayout{

    private Button left;
    private TextView title;
    private Button right;

    private LayoutParams leftParams;
    private LayoutParams titleParams;
    private LayoutParams rightParams;

    private int titleWidth;
    private int titleHeight;

    private WindowManager wm ;

    public TitleBar(Context context) {
        super(context);
        initView(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){

        wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

        left = new Button(context);
        left.setText("←");
        left.setBackgroundColor(Color.TRANSPARENT);
        leftParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT );
        leftParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT,TRUE);
        addView(left,leftParams);

        title = new TextView(context);
        title.setText("标题");
        titleParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        titleParams.addRule(RelativeLayout.CENTER_IN_PARENT,TRUE);
        addView(title,titleParams);

        right = new Button(context);
        right.setText("···");
        right.setBackgroundColor(Color.TRANSPARENT);
        rightParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        rightParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,TRUE);
        addView(right,rightParams);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //获取宽度的模式和大小
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        //获取高度的模式和大小
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        this.titleHeight = measureHeight(heightMode,height);
        this.titleWidth = measureWidth(widthMode,width);

        this.setMeasuredDimension(titleWidth,titleHeight);
    }

    private int measureWidth(int mode ,int width){
        switch(mode){
            case MeasureSpec.UNSPECIFIED:
                break;
            case MeasureSpec.AT_MOST://warp_content
                DisplayMetrics dm = new DisplayMetrics();
                wm.getDefaultDisplay().getMetrics(dm);
                titleWidth = dm.widthPixels;
                break;
            case MeasureSpec.EXACTLY://match_parent或者具体的数字
                titleWidth = width;
                break;
        }
        return this.titleWidth;
    }

    private int measureHeight(int mode , int height){
        switch(mode){
            case MeasureSpec.UNSPECIFIED:
                break;
            case MeasureSpec.AT_MOST:
                titleHeight = dp2px(50);
                break;
            case MeasureSpec.EXACTLY:
                titleHeight = height;
                break;
        }
        return this.titleHeight;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
}
