package password.hb.com.password.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import date.hb.com.wdiget.TitleBarI;
import password.hb.com.password.R;
import password.hb.com.password.widget.TitleBar;

/**
 * Created by A on 2017/3/7.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected TitleBarI titlebar;

    private FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onInitView(savedInstanceState);
        onSetEvent();
        onInitData();
    }

    public void setContentViewWithActionBar(int layoutResID){
        setContentView(R.layout.base_activity);
        titlebar = (TitleBarI) findViewById(R.id.titlebar);
        container = (FrameLayout) findViewById(R.id.container);

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(layoutResID,container,true);
    }

    public TitleBarI getTitlebar(){
        return this.titlebar;
    }

    protected abstract void onInitView(Bundle args0);
    protected abstract void onSetEvent();
    protected abstract void onInitData();


}
