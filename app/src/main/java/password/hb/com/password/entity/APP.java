package password.hb.com.password.entity;

import com.activeandroid.Model;

/**
 * Created by A on 2017/1/13.
 */


public class APP {

    private String name;
    private int imageId;

    public APP(String name,int imageId){
        this.name = name;
        this.imageId = imageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
