package password.hb.com.password.constant;

/**
 * Created by A on 2017/1/13.
 * 数据传递的常量
 */

public class DeliverConstant {

    public static final String password = "password";//传递password对象
    public static final String psswordId = "passwordId";//传递password对象的ID
    public static final String psswordIndex = "passwordIndex";//传递password对象的index

    public static final String resetOrSetting = "resetOrSetting";
    public static final String resetPassword = "resetPassword";
    public static final String settingPassword = "settingPassword";

}
