package password.hb.com.password.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.lang.reflect.Field;

/**
 * Created by A on 2016/12/5.
 */

public class SharedPreUtil {
    private SharedPreferences sharedPreferences;

    public SharedPreUtil(Context context){
        this.sharedPreferences=context.getSharedPreferences("APP-name",Context.MODE_PRIVATE);
    }

    public void save(Object obj,Class c){
        Field[] fs = c.getDeclaredFields();
        for ( int i = 0 ; i < fs. length ; i++) {
            try {
                Field f = fs[i];
                f.setAccessible(true); // 设置些属性是可以访问的
                Object val = f.get(obj); // 得到此属性的值
                String type = f.getType().toString(); // 得到此属性的类型
                String name = f.getName();// 得到属性的名称
                saveByType(type,name,val);//保存
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Object get(Class c){
        try {
            Object obj = Class.forName(c.getName()).newInstance();
            Field[] fs = c.getDeclaredFields();
            for ( int i = 0 ; i < fs. length ; i++) {
                Field f = fs[i];
                f.setAccessible(true); // 设置些属性是可以访问的
                String type = f.getType().toString(); // 得到此属性的类型
                String name = f.getName(); //得到此属性的名称
                Object value = this.getByType(type,name);//得到属性的值
                f.set(obj,value);//设置此属性的值
            }
            return obj;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private Object getByType(String type ,String key){
        Object obj=null;
        if(type.endsWith("String")){
            obj=sharedPreferences.getString(key, null);
        }else if(type.endsWith("int")||type.endsWith("Integer")){
            obj=sharedPreferences.getInt(key,-100000);
        }else if(type.endsWith("boolean")){
            obj=sharedPreferences.getBoolean(key,false);
        }else if(type.endsWith("Long")||type.endsWith("long")){
            obj=sharedPreferences.getLong(key,-10000);
        }else if(type.endsWith("Float")||type.endsWith("float")){
            obj=sharedPreferences.getFloat(key,-10000);
        }
        return obj;
    }

    private void saveByType(String type,String key,Object val){
        if(type.endsWith("String")){
            sharedPreferences.edit().putString(key,(String)val).commit();
        }else if(type.endsWith("int")||type.endsWith("Integer")){
            sharedPreferences.edit().putInt(key,(int)val).commit();
        }else if(type.endsWith("boolean")){
            sharedPreferences.edit().putBoolean(key,(boolean)val).commit();
        }else if(type.endsWith("Long")||type.endsWith("long")){
            sharedPreferences.edit().putLong(key,(Long)val).commit();
        }else if(type.endsWith("Float")||type.endsWith("float")){
            sharedPreferences.edit().putFloat(key,(Float)val).commit();
        }else{
            Log.d("Exception","类型无法识别");
        }
    }
}
