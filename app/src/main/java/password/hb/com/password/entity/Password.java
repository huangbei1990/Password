package password.hb.com.password.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import java.io.Serializable;
/**
 * Created by A on 2016/12/5.
 */

@Table(name = "Password")
public class Password extends Model implements Serializable{

    @Column(name = "appName")
    private String appName;//应用名称

    @Column(name = "appImageId")
    private int appImageId;//应用图片id

    @Column(name = "userName")
    private String userName;//用户名

    @Column(name = "password")
    private String password;//密码

    @Column(name = "tag")
    private String tag;//备注

    public Password(){ }

    public Password(String appName,String userName,String password,int appImageId){
        this.appName = appName;
        this.userName = userName;
        this.password = password;
        this.appImageId = appImageId;
    }

    public Password(String appName, String userName, String password, int appImageId,String tag) {
        this.appName = appName;
        this.userName = userName;
        this.password = password;
        this.appImageId = appImageId;
        this.tag = tag;
    }

    public String getAppName() {
        return appName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getTag() {
        return tag;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getAppImageId() {
        return appImageId;
    }

    public void setAppImageId(int appImageId) {
        this.appImageId = appImageId;
    }

    public boolean isSameModel(Password model) {
        if(this.getId() == model.getId()){
            return true;
        }else {
            return false;
        }
    }

    public boolean isSameModel(long mid){
        if(this.getId() == mid){
            return true;
        }else{
            return false;
        }
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("应用名称：");
        builder.append(appName);
        builder.append("\n");
        builder.append("用户名：");
        builder.append(userName);
        builder.append("\n");
        builder.append("密码：");
        builder.append(password);
        builder.append("\n");
        if(!(tag==null || tag.equals(""))){
            builder.append("备注：");
            builder.append(tag);
            builder.append("\n");
        }
        return builder.toString();
    }
}
