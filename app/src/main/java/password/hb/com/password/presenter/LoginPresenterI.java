package password.hb.com.password.presenter;

import password.hb.com.password.ui.LoginViewI;

/**
 * Created by A on 2017/3/6.
 */

public interface LoginPresenterI {
    public void login(String password,LoginViewI loginview);
}
